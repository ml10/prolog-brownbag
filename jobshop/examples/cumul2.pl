:- use_module(library(clpfd)).
:- use_module(library(lists), [append/3]).

schedule(End) :-
	Tasks = [task(S1,16,_,2,_),
	task(S2,6,_,9,_),
	task(S3,13,_,3,_),
	task(S4,7,_,7,_),
	task(S5,5,_,10,_),
	task(S6,18,_,1,_),
	task(S7,4,_,10,_)].
	%length(Ss, 7),
	%Ss ins 1..30,
	End in 1..50,
	%after(Ss, Ds, End),
	cumulative(Tasks, 13),
	%append(Ss, [End], Vars),
	once(labeling([min(End)], Tasks)).

%after([], [], _).
%after([S|Ss], [D|Ds], E) :-
%	E #>= S+D, after(Ss, Ds, E).
