#!/usr/bin/swipl -q -f
 
subset([], []).
subset([E|Tail], [E|NTail]):-subset(Tail, NTail).
subset([_|Tail], NTail):- subset(Tail, NTail).
 
size([],0).
size([_|T],N):-
    size(T,M),
    N is M+1.
 
validate(X) :- size(X, Y), Y < 5 -> write(X).
 
main :- set_prolog_flag(toplevel_print_options,
                [quoted(true)]),
	subset(['A', 'A#', 'B', 'C', 'C#', 'D', 
                'D#', 'E', 'F', 'F#', 'G', 'G#'], X),
	validate(X),
	nl,
	fail,
	true.
 
run :- main, halt.
