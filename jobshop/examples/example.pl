:- use_module(library(clpfd)).

pred(Vs,C) :-
	sum(Vs, #=, C), 
	labeling([min(C)], Vs).

example(Ls) :-
	Ls = [_,_,_],
	Ls ins 1..10,
	pred(Ls, C), 
	labeling([min(C)], Ls).
