:- use_module(library(clpfd)).

schedule(Ss, End) :-
    Ss = [S1,S2,S3,S4,S5,S6,S7],
    Es = [E1,E2,E3,E4,E5,E6,E7],
    Tasks = [task(S1,16,E1, 2,0),
        task(S2, 6,E2, 9,0),
        task(S3,13,E3, 3,0),
        task(S4, 7,E4, 7,0),
        task(S5, 5,E5,10,0),
        task(S6,18,E6, 1,0),
        task(S7, 4,E7,11,0)],
    Ss ins 1..30,
    Es ins 1..50,
    End in 1..50,
    max_member(End, Es),
    cumulative(Tasks, [limit(13)]),
    append(Ss, [End], Vars),
    labeling([min(End)], Vars).

