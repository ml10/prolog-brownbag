:- use_module(library(clpfd)).
 
% task list
task(1, 8).
task(2, 6).
task(3, 7).
task(4, 5).
task(5, 2).
task(6, 3).
task(7, 8).
task(8, 6).
task(9, 2).
task(10, 6).
task(11, 1).
task(12, 2).
task(13, 6).
task(14, 4).

addUp([], [], _).
addUp([S|Ss], [E|Es], P) :-
	task(P, D),
	E #= (S + D),
	P2 is (P + 1),
	addUp(Ss, Es, P2).

min([X],X):-!.
	min([X|Xs],X):- min(Xs,Y), X #=<Y.
	min([X|Xs],N):- min(Xs,N), N #<X.

max([X],X):-!.
	max([X|Xs],X):- max(Xs,Y), X #>=Y.
	max([X|Xs],N):- max(Xs,N), N #>X.

test:-
	START = [S1,S2,S3,S4,S5],
	START ins 0 .. 66,
	
	addUp(START, END, 1),
	
	min(START, A),
	max(END, B),
	
	labeling([min(B)], START),  
	write(START), nl, write(END), nl,
	write(A), write('  '), write(B), !.


