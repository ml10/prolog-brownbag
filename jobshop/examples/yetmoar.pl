:- use_module(library(clpfd)). % Load the FD solver

% Set constraints 
constraints(Week, Fun) :- 
        Week = [M, T, W, H, F, S, U],
        Week ins 0..30,
        % Workers for Monday come from Monday, Sunday, Saturday, Friday, Thursday
        M + U + S + F + H #>= 17,
        % Workers from Tuesday come from Tuesday, Monday, ...
        T + M + U + S + F #>= 13,
        W + T + M + U + S #>= 15,
        H + W + T + M + U #>= 19,
        F + H + W + T + M #>= 14, 
        S + F + H + W + T #>= 16,
        U + S + F + H + W #>= 11,
        % Goal function is the sum of full-time workers during whole week
        Fun in 0..30,
        Fun #= M + T + W + H + F + S + U.

% Finds the minimum amount of full time workers (Fun) needed to operate post office
top(Fun) :-
        constraints(Week, Fun), 
        once(labeling([min(Fun)], Week)).
% Find a solution with minimum workers during the week 
% and also minimum workers during weekend 
top :-
        top(MinValue),
        constraints(Week, FunAll), 
        FunAll #= MinVal,
        Week = [M, T, W, H, F, S, U],
        Fun #= S + U, 
        once(labeling([min(Fun)], Week)),
        write('Solution which minimizes the number of employees for Sat+Sun'), nl,
        write([M, T, W, H, F, S, U]), nl, 
        write('Number of employees for weekend is '), 
        write(Fun), nl. 
