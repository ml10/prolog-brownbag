data(
   % First argument is a list of tasks with name, duration and machine and start time (unconstrained)
   [
     task(ad1_copy, 4, copier, AC1),
     task(ad1_sort, 4, sorter, AS1),
     task(ad1_pack, 3, packager, AD1),
     task(ad2_copy, 5, copier, AC2),
     task(ad2_sort, 4, sorter, AS2),
     task(ad2_pack, 4, packager, AD2)
   ],
   % second argument is a list of lists of task names to be performed in order
   [[ad1_copy,ad1_sort,ad1_pack],
    [ad2_copy,ad2_sort,ad2_pack]
   ],
   % third argument is the list of different machines
   [copier,sorter,packager],
   % fourth argument is the list of all start time variables
   [AC1,AS1,AD1,AC2,AS2,AD2]
).
