% This is a program stub for the job shop scheduling problem
% given in the project.

% Load in finite domain module
:- use_module(library(clpfd)).
% Other modules you may use are loaded here

% You must use the following to load in the data
:-['job.pl'].

% Your definition of the userdefined constraint jobshop(L) should be here.

jobshop(L) :-
	data(W,X,_,Z),
	init(W),
	precLL(X,W),
	capacity(W),
	labeling([],Z),
	L = Z.

init([]).
init(TASKLIST):-
	TASKLIST=[task(_,D,_,ST)|T],
	ST in 0..48,
	ST+D #=< 48,
	init(T).

precLL([], _).
precLL([H|T],TASKLIST) :-
	precL(H, [2], TASKLIST),
	precLL(T,TASKLIST).

precL(_, [], _).
precL([], _, _).
precL([H|T], _, TASKLIST) :-
	T=[HH|TT],
	member(task(H,D1,_,T1), TASKLIST),
	member(task(HH,_,_,T2), TASKLIST),
	T1 + D1 #< T2,
	write(H), write('-'), write(HH),write('\n'),
	precL(T, TT,TASKLIST).

exlude(T1, D1, T2, _) :-
	T1 + D1 #< T2.
exlude(T1, _, T2, D2) :-
	T2 + D2 #< T1.

capacity([]).
capacity(TASKLIST) :-
	TASKLIST = [task(J,D,M,_)|T],
	write(J), write('-'), write(D), write('-'), write(M), write('-'), write(ST), write('\n'),
	nonoverlaps(J,D,M,T,TASKLIST),
	capacity(T).

nonoverlaps(_,_,_,[],_).
nonoverlaps(J,D,M,L,TASKLIST) :-
	write('foo\n'),
	L = [task(_, _, M2, _) | T],
	write('bar\n'),
	write(M), write('-'), write(M2), write('\n'),
	M = M2,
	write('baaz\n'),
	nonoverlaps(J,D,M,T,TASKLIST).
nonoverlaps(J,D,M,L,TASKLIST) :-
	write('quux\n'),
	L=[task(_,D2,M2,T2)|T],
	write('barney\n'),
	write(M), write('-'), write(M2), write('\n'),
	M = M2,
	write('fred\n'),
	member(task(J,D,M,T1),TASKLIST),
	write('wilma\n'),
	exlude(T1,D,T2,D2),
	write('betty\n'),
	nonoverlaps(J,D,M,T,TASKLIST).

% Your rules should not go below this line

% Don't delete or comment away the following

% Output the solution
myoutput :- jobshop(L),
 	    write('The starting time of the tasks are: n'),
            myprint(L, 1).

% this is to output the sarting time
myprint([], _).
myprint([X|L], I) :- write('task '), write(I), write(' starts at '),
                   write(X), write('n'), succ(I, Y), myprint(L, Y).

% initiat the output.
:- myoutput.
