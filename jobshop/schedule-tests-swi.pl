
:- use_module(library(clpfd)).

:- ['test_tasks'].

tests(Tasks, End) :-
	test_tasks(Tasks,Starts,Ends,Bound,Buckets),
	Starts ins 0..Bound,
	Ends ins 0..Bound,
	End in 0..Bound,
	min_member(End, Ends),
	cumulative(Tasks, [limit(Buckets)]), 
	append(Starts, [End], Vars),
	once(labeling([max(End)], Vars)).
