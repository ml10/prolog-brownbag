
:- use_module(library(clpfd)).

:- ['test_tasks'].

%tests(Tasks, End) :-
schedule_tests(Tasks) :-
	test_tasks(Tasks,Starts,Ends,Bound,Buckets),
	domain(Starts,0,Bound),
	domain(Ends,0,Bound),
	%domain([End],0,Bound),
	%maximum(End, Ends),
	cumulative(Tasks, [limit(Buckets)]),
	%append(Starts, [End], Vars),
	%labeling([minimize(End)], Vars).
	labeling([min], Ends).
