:- use_module(library(clpfd)).

test_tasks(Tasks,Starts,Ends,Bound,Buckets) :-
  Starts = [S1,S2,S3,S4,S5,S6,S7],
  Ends = [E1,E2,E3,E4,E5,E6,E7],
  Bound = 8000,
  Buckets = 3,
  Tasks = [
    task(S1,397,E1,1,'integration.com.hcsc.integratedquote.AgentTest'),
    task(S2, 30,E2,1,'integration.com.hcsc.integratedquote.ApplicationsInProgressTest'),
    task(S3, 87,E3,1,'integration.com.hcsc.integratedquote.AgentQuoteTest'),
    task(S4,454,E4,1,'integration.com.hcsc.integratedquote.AccountPageTest'),
    task(S5, 36,E5,1,'integration.com.hcsc.integratedquote.CampaignTest'),
    task(S6,116,E6,1,'integration.com.hcsc.integratedquote.CensusEffectiveDateTest'),
    task(S7, 28,E7,1,'integration.com.hcsc.integratedquote.CatastrophicPlanTest')].

