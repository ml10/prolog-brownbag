Given the set S (of natural numbers)
find Sets A, B, C such that:
 A U B U C = S
 max of (sum(A), sum(B), sum(C)) is minimized
