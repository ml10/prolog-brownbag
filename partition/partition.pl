use_module(library(clpfd)).

partition(In, A, B, C) :-
  union(A, B, D),
  union(C, D, In),
  sum_list(A, ASum),
  sum_list(B, BSum),
  sum_list(C, CSum),
  max_list([ASum, BSum, CSum], ListOfSums),
  minimize(ListOfSums).
