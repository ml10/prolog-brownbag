% zebra puzzle aka Einstein puzzle

% There are five houses.
% The Englishman lives in the red house.
% The Spaniard owns the dog.
% Coffee is drunk in the green house.
% The Ukrainian drinks tea.
% The green house is immediately to the right of the ivory house.
% The Old Gold smoker owns snails.
% Kools are smoked in the yellow house.
% Milk is drunk in the middle house.
% The Norwegian lives in the first house.
% The man who smokes Chesterfields lives in the house next to the man with the fox.
% Kools are smoked in the house next to the house where the horse is kept.
% The Lucky Strike smoker drinks orange juice.
% The Japanese smokes Parliaments.
% The Norwegian lives next to the blue house.
% 
% Now, who drinks water? Who owns the zebra?
% 
% In the interest of clarity, it must be added that:
% each of the five houses is painted a different color, 
% and their inhabitants are of different national extractions, 
% own different pets, 
% drink different beverages 
% and smoke different brands of American cigarets [sic].
% One other thing: in statement 6, right means your right.
% — Life International, December 17, 1962

member_of(House, [House | _]).
member_of(House, [_ | Tail]) :-
	member_of(House, Tail).

right_of(H1, H2, [H2, H1 | _]).
right_of(H1, H2, [_ | Tail]) :-
	right_of(H1, H2, Tail).

next_to(H1, H2, S) :-
	right_of(H1, H2, S).
next_to(H1, H2, S) :-
	right_of(H2, H1, S).

solve(Street) :-

	%Street = [(C1,norwegian,P1,D1,S1), (C2,N2,P2,D2,S2), (C3,N3,P3,milk,S3), (C4,N4,P4,D4,S4), (C5,N5,P5,D5,S5)],
	Street = [(_,norwegian,_,_,_), (_,_,_,_,_), (_,_,_,milk,_), (_,_,_,_,_), (_,_,_,_,_)],

	member_of((red, englishman, _, _, _), Street),
	member_of((_, spaniard, dog, _, _), Street),
	member_of((green, _, _, coffee, _), Street),
	member_of((_, ukranian, _, tea, _), Street),
	right_of((green, _, _, _, _), (ivory, _, _, _, _), Street),
	member_of((_, _, snails, _, oldgold), Street),
	member_of((yellow, _, _, _, kools), Street),
	next_to((_, _, _, _, chesterfields), (_, _, fox, _, _), Street),
	next_to((_, _, _, _, kools), (_, _, horse, _, _), Street),
	member_of((_, _, _, oj, luckystrikes), Street),
	member_of((_, japanese, _, _, parliaments), Street),
	next_to((_, norwegian, _, _, _), (blue, _, _, _, _), Street),
	member_of((_, _, zebra, _, _), Street),
	member_of((_, _, _, water, _), Street).

show :-
	
	Street = [(C1,N1,P1,D1,S1), (C2,N2,P2,D2,S2), (C3,N3,P3,D3,S3), (C4,N4,P4,D4,S4), (C5,N5,P5,D5,S5)],

	solve(Street),

	writef('┌─%r─┬─%r─┬─%r─┬─%r─┬─%r─┬─%r─┐\n', ['─', 11, '─', 9, '─', 13,'─', 10,'─', 12,'─', 11]),
	writef('│%13c│%11c│%15c│%12c│%14c│%13c│\n', ['house',		1,2,3,4,5]),
	writef('├─%r─┼─%r─┼─%r─┼─%r─┼─%r─┼─%r─┤\n', ['─', 11, '─', 9, '─', 13,'─', 10,'─', 12,'─', 11]),
	writef('│%13c│%11c│%15c│%12c│%14c│%13c│\n', ['color',		C1,C2,C3,C4,C5]),
	writef('├─%r─┼─%r─┼─%r─┼─%r─┼─%r─┼─%r─┤\n', ['─', 11, '─', 9, '─', 13,'─', 10,'─', 12,'─', 11]),
	writef('│%13c│%11c│%15c│%12c│%14c│%13c│\n', ['nationality', 	N1,N2,N3,N4,N5]),
	writef('├─%r─┼─%r─┼─%r─┼─%r─┼─%r─┼─%r─┤\n', ['─', 11, '─', 9, '─', 13,'─', 10,'─', 12,'─', 11]),
	writef('│%13c│%11c│%15c│%12c│%14c│%13c│\n', ['pet',		P1,P2,P3,P4,P5]),
	writef('├─%r─┼─%r─┼─%r─┼─%r─┼─%r─┼─%r─┤\n', ['─', 11, '─', 9, '─', 13,'─', 10,'─', 12,'─', 11]),
	writef('│%13c│%11c│%15c│%12c│%14c│%13c│\n', ['drink',		D1,D2,D3,D4,D5]),
	writef('├─%r─┼─%r─┼─%r─┼─%r─┼─%r─┼─%r─┤\n', ['─', 11, '─', 9, '─', 13,'─', 10,'─', 12,'─', 11]),
	writef('│%13c│%11c│%15c│%12c│%14c│%13c│\n', ['smoke',		S1,S2,S3,S4,S5]),
	writef('└─%r─┴─%r─┴─%r─┴─%r─┴─%r─┴─%r─┘\n', ['─', 11, '─', 9, '─', 13,'─', 10,'─', 12,'─', 11]).
