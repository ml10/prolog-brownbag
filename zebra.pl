% zebra puzzle aka Einstein puzzle

% There are five houses.
% The Englishman lives in the red house.
% The Spaniard owns the dog.
% Coffee is drunk in the green house.
% The Ukrainian drinks tea.
% The green house is immediately to the right of the ivory house.
% The Old Gold smoker owns snails.
% Kools are smoked in the yellow house.
% Milk is drunk in the middle house.
% The Norwegian lives in the first house.
% The man who smokes Chesterfields lives in the house next to the man with the fox.
% Kools are smoked in the house next to the house where the horse is kept.
% The Lucky Strike smoker drinks orange juice.
% The Japanese smokes Parliaments.
% The Norwegian lives next to the blue house.
% 
% Now, who drinks water? Who owns the zebra?
% 
% In the interest of clarity, it must be added that:
% each of the five houses is painted a different color, 
% and their inhabitants are of different national extractions, 
% own different pets, 
% drink different beverages 
% and smoke different brands of American cigarets [sic].
% One other thing: in statement 6, right means your right.
% — Life International, December 17, 1962


show :-
	
	Street = [(C1,N1,P1,D1,S1), (C2,N2,P2,D2,S2), (C3,N3,P3,D3,S3), (C4,N4,P4,D4,S4), (C5,N5,P5,D5,S5)],

	solve(Street),

	writef('┌─%r─┬─%r─┬─%r─┬─%r─┬─%r─┬─%r─┐\n', ['─', 11, '─', 9, '─', 13,'─', 10,'─', 12,'─', 11]),
	writef('│%13c│%11c│%15c│%12c│%14c│%13c│\n', ['house',		1,2,3,4,5]),
	writef('├─%r─┼─%r─┼─%r─┼─%r─┼─%r─┼─%r─┤\n', ['─', 11, '─', 9, '─', 13,'─', 10,'─', 12,'─', 11]),
	writef('│%13c│%11c│%15c│%12c│%14c│%13c│\n', ['color',		C1,C2,C3,C4,C5]),
	writef('├─%r─┼─%r─┼─%r─┼─%r─┼─%r─┼─%r─┤\n', ['─', 11, '─', 9, '─', 13,'─', 10,'─', 12,'─', 11]),
	writef('│%13c│%11c│%15c│%12c│%14c│%13c│\n', ['nationality', 	N1,N2,N3,N4,N5]),
	writef('├─%r─┼─%r─┼─%r─┼─%r─┼─%r─┼─%r─┤\n', ['─', 11, '─', 9, '─', 13,'─', 10,'─', 12,'─', 11]),
	writef('│%13c│%11c│%15c│%12c│%14c│%13c│\n', ['pet',		P1,P2,P3,P4,P5]),
	writef('├─%r─┼─%r─┼─%r─┼─%r─┼─%r─┼─%r─┤\n', ['─', 11, '─', 9, '─', 13,'─', 10,'─', 12,'─', 11]),
	writef('│%13c│%11c│%15c│%12c│%14c│%13c│\n', ['drink',		D1,D2,D3,D4,D5]),
	writef('├─%r─┼─%r─┼─%r─┼─%r─┼─%r─┼─%r─┤\n', ['─', 11, '─', 9, '─', 13,'─', 10,'─', 12,'─', 11]),
	writef('│%13c│%11c│%15c│%12c│%14c│%13c│\n', ['smoke',		S1,S2,S3,S4,S5]),
	writef('└─%r─┴─%r─┴─%r─┴─%r─┴─%r─┴─%r─┘\n', ['─', 11, '─', 9, '─', 13,'─', 10,'─', 12,'─', 11]).
